"""
This is  a revised approach to controlling Arduino devices through serial port communication.
While effective for certain applications, it's important to note that serial communication has its limitations.
Primarily, the necessity for physical connections can be impractical in environments where wiring is undesirable
or unfeasible.
Additionally, serial connections typically lack encryption, presenting a security concern,
especially since Arduino devices have limited support for encrypted communications due to their constrained processing
capabilities. Despite these limitations, serial port communication remains a valuable tool for those looking to utilize
Arduino devices, particularly for testing purposes within secure environments. Users should exercise caution, however,
as physical access to the device could allow unauthorized individuals to easily connect to and control the Arduino.

This module may be removed in the future. Since we are working on new approach that will Ethernet based communication (LAN)

"""
import time
from abc import ABC
from datetime import datetime
import serial.tools.list_ports
import serial

from Device.core.Controller import Controller
from Device.core.device import Register, RequiredArgs

ARDUINO_CONFIGS = {
    'uno': {
        'digital_pins': range(0, 14),
        'analog_input_pins': range(0, 6),
        'pwm_pins': [3, 5, 6, 9, 10, 11],
    },
    'mega': {
        'digital_pins': range(0, 54),
        'analog_input_pins': range(0, 16),
        'pwm_pins': [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 44, 45, 46],
    },
    'leonardo': {
        'digital_pins': range(0, 20),
        'analog_input_pins': range(0, 12),
        'pwm_pins': [3, 5, 6, 9, 10, 11, 13],
    },
    'nano': {
        'digital_pins': range(0, 14),
        'analog_input_pins': range(0, 8),
        'pwm_pins': [3, 5, 6, 9, 10, 11],
        # Add configurations for other models as needed
    }
}


@Register.action
class ArduinoControllerSerial(Controller):
    # This is a trusted class. There is no Need to implement URLS
    actions = []  # Not needed to declare but for safety purposes

    @staticmethod
    def list_serial_ports():
        """ Lists serial port names
            :returns:
                A list of the serial ports available on the system
        """
        available_ports = list(serial.tools.list_ports.comports())
        return available_ports

    def __init__(self, **kwargs):
        """
        Initializes a new instance of the device, optionally establishing a serial connection upon instantiation.

        Parameters:
            port (str): The serial port to connect to. Example: 'COM3' on Windows or '/dev/ttyACM0' on Unix.
            init_on_wake (bool, optional): If True, automatically initializes a serial connection to the Arduino
                                           when the instance is created. Defaults to True.
            **kwargs: Arbitrary keyword arguments. Supported keywords include:
                baud_rate (int, optional): The baud rate for serial communication. Defaults to 9600.
                timeout (int, optional): The timeout in seconds for serial read operations. Defaults to 2.
                sleep_timer (int, optional): The time in seconds to wait after initializing the serial connection,
                                             allowing the device to wake up. Defaults to 3.
                rw_interval (float, optional): The read/write interval in seconds, specifying how long to wait
                                               between write and subsequent read operations. Defaults to 0.1.

        Attributes:
            action (list): A list of supported actions, e.g., 'turn_on', 'turn_off', 'get_status', 'get_sensor_value'.
            serial_object (serial.Serial or None): The serial connection object. None if the connection is not
                                                   established or if 'init_on_wake' is False.

        Note:
            The serial connection uses unencrypted communication, which may not be suitable for all applications.
            Use caution if physical access to the device is a concern, as it could potentially allow unauthorized
            command execution on the Arduino. Encrypted communication may be implemented in future versions of code.
        """
        super().__init__(**kwargs)
        self.port = kwargs.get('port', None)
        self.baud_rate = kwargs.get('baud_rate', 9600)
        self.timeout = kwargs.get('timeout', 2)
        self.model = kwargs.get('model', 'Uno')
        self.sleep_timer = kwargs.get('sleep_timer', 3)
        self.rw_interval = kwargs.get('rw_interval', 0.1)
        self.serial_object = None
        self.model = kwargs.get('model', 'Uno')
        if self.model not in ARDUINO_CONFIGS:
            raise ValueError(f"Model '{self.model}' is not supported.")
        self.config = ARDUINO_CONFIGS[self.model]
        self.init_on_wake = kwargs.get('init_on_wake', True)
        if self.init_on_wake:
            self.initialize_connection()

    @Register.ignore
    def validate_pin(self, pin, pin_type='digital'):
        """
        Validates that the pin is appropriate for the action and the board model.

        Parameters:
            pin (int): The pin number to validate.
            pin_type (str): The type of pin ('digital', 'analog_input', or 'pwm').

        Raises:
            ValueError: If the pin number is not valid for the specified type and board model.
        """
        valid_pins = self.config.get(f'{pin_type}_pins', [])
        if pin not in valid_pins:
            raise ValueError(f"Pin {pin} is not valid for {pin_type} operations on {self.model}.")

    @staticmethod
    @Register.ignore
    def required_arguments():
        """Defines the required arguments for ArduinoController class."""
        return super().required_arguments() + [
            RequiredArgs(name='port', description='Serial Port', default_value=None, required=True),
            RequiredArgs(name='baud_rate', description='Baud Rate for serial communication', default_value=9600,
                         required=False),
            RequiredArgs(name='rw_interval', description='Read Write Interval in seconds', default_value=0.1,
                         required=False),
            RequiredArgs(name='model', description='Arduino board model', default_value='uno', required=False),
            RequiredArgs(name='sleep_timer', description='Time to wait for Arduino to start', default_value=3,
                         required=False),
        ]

    @Register.ignore
    def initialize_connection(self, force_reconnect=False):
        """Initializes or reinitializes the serial connection to Arduino.

        :param force_reconnect: If True, forces the reinitialization of the serial connection,
                                even if it's already open.
        """
        # Check if we already have an open connection
        if self.serial_object and self.serial_object.isOpen() and not force_reconnect:
            print("Serial connection already established.")
            return True

        # Close the existing connection if force_reconnect is True
        if self.serial_object and force_reconnect:
            self.close_connection()

        try:
            self.serial_object = serial.Serial(
                self.port, baudrate=self.baud_rate, timeout=self.timeout
            )

            time.sleep(self.sleep_timer)  # Wait for the connection to settle
            print(f"Connected to {self.port} successfully.")
            return True
        except serial.SerialException as e:
            print(f"Cannot connect to device on port {self.port}. Error: {e}")
            return False

    @Register.ignore
    def write(self, data_string,args:dict=None):
        """
        A function to send a string to Arduino.
        :param: data_string String
        :returns: String, Boolean
        """
        if not self.serial_object:
            print("Serial connection not initialized.")
            return None, False
        try:

            self.serial_object.write(data_string.encode())
            time.sleep(self.rw_interval)

            response = self.serial_object.readline().decode('utf-8').strip()

            return response, True
        except Exception as e:
            print(f"Error communicating with device: {e}")
            return None, False

    def turn_on(self, args: dict):
        """
        Turn on the device at the pin specified.
        :param args: Dictionary tuple containing information about thed device to be turned on.
        :return: string response given by Arduino or None if the Arduino is not initialized.
        """
        pin:int = args.get('pin', -1)
        if pin is None or pin == -1:
            return {'response': 'Pin Value Not set.'}
        self.validate_pin(pin, 'digital')
        if self.serial_object is not None:
            command = str(pin) + ":on"

            response = self.serial_object.write(command.encode())
            if response is not None:
                return {response: 'response', 'device': {'state': 'on', 'updated_on': datetime.now()}}
        return None

    def turn_off(self, args: dict):
        """
        Turn on the device at the pin specified.
        :param args: Dictionary tuple containing information about thed device to be turned on.
        :return: string response given by Arduino or None if the Arduino is not initialized.
        """
        pin = args.get('pin', -1)
        if pin is None or pin == -1:
            return {'response': 'Pin Value Not set.'}
        self.validate_pin(pin, 'digital')
        if self.serial_object is not None:
            command = str(pin) + ":off"

            response = self.serial_object.write(command.encode())
            if response is not None:
                return {response: 'response', 'device': {'state': 'on', 'updated_on': datetime.now()}}
        return None

    def set_value(self, args: dict):
        """
        Sts the output level for a device connected to a specified pin.
        This can control various device features such as brightness, speed, etc.

        :param args: Dictionary containing information about the device to be controlled,
                     including 'pin' and 'output_level' keys.
        :return: Response dictionary with the execution result.

        """
        pin, value = args.get('pin', None), args.get('output_level', None)
        if pin is None or value is None:
            return {'response': 'Invalid Arguments'}

        if self.serial_object is not None:

            response, flag = self.serial_object.write(f"{pin}:{value}".encode())
            if response is not None:
                return response
        return None

    def get_value(self, args: dict):
        """
        Turn on the device at the pin specified.
        :param args: Dictionary tuple containing information about thed device to be turned on.
        :return: string response given by Arduino or None if the Arduino is not initialized.
        """
        pin = args.get('pin', -1)
        if pin is not None or pin == -1:
            return {'response': 'Pin Value Not set.'}
        if self.serial_object is not None:
            command = str(pin) + ":read"

            response = self.serial_object.write(command.encode())
            if response is not None:
                return {response: 'response', 'device': {'state': 'on', 'updated_on': datetime.now()}}
        return None

    @Register.ignore
    def read_line(self, args: dict = None):
        return self.serial_object.read_line().decode().strip()


    def read(self, data_string, args: dict = None):
        return self.read_line()


    @Register.ignore
    def close_connection(self):
        """Closes the serial connection.
        :return: None"""
        if self.serial_object and self.serial_object.isOpen():
            self.serial_object.close()
            self.serial_object = None
            print("Serial connection closed.")


class ArduinoControllerLAN(Controller, ABC):
    """Future Implementation"""
    pass