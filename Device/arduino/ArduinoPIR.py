from Device.arduino.ArduinoController import ArduinoControllerSerial
from Device.core.device import Device, RequiredArgs


class ArduinoPIR(Device):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.category = "Sensor"
        self.is_controller = False
        self.pin = kwargs.get('pin', None)
        self.controller_id = kwargs.get('controller_id', None)
        if self.pin is None:
            raise ValueError('Pin Number is expected, Check the database entry.')
        if self.controller_id is None:
            raise ValueError('Controller ID is expected, Check the database entry.')

    @staticmethod
    def required_arguments():
        return super().required_arguments() + [RequiredArgs('status', 'The status of the PIR', 'off', True)]
