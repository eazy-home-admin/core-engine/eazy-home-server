from Device.arduino.ArduinoController import ArduinoControllerSerial
from Device.core.device import Device, RequiredArgs


class ArduinoTemperatureSensor(Device):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.category = "Sensor"
        self.is_controller = False
        self.pin = kwargs.get('pin', None)
        self.controller_id= kwargs.get('controller_id', None)
        if self.pin is None:
            raise ValueError('Pin Number is expected, Check the database entry.')
        if self.controller_id is None:
            raise ValueError('Controller ID is expected, Check the database entry.')

    @staticmethod
    def required_arguments():
        return [
            RequiredArgs(name='pin', description='Pin Number', default_value=None, required=True),
            RequiredArgs(name='temperature', description='Temperature Reading', default_value=None, required=False),
            RequiredArgs(name='humidity', description='Humidity Reading', default_value=None, required=False),
        ]


