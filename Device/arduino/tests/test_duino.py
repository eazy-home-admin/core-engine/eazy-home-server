import unittest
from serial import Serial
from Device.arduino.ArduinoController import ArduinoControllerSerial as Arduino
from Device.arduino.simulators import ArduinoSimulator


class MyTestCase(unittest.TestCase):
    def test_ser(self):
        arduino_sim = ArduinoSimulator()
        arduino_sim.run_serial_threaded()

        m_name,s_name = arduino_sim.get_tty_name()
        print(s_name)
        # ser = Serial(s_name, 9600, timeout=1)
        # ser.write(b'1:on\r\n')
        # print("result: %s" % ser.readline().decode())
        # ser.write(b'read:1\r\n')
        # print("result: %s" % ser.readline().decode())
        # ser.write(b'1:off\r\n')
        # print("result: %s" % ser.readline().decode())
        arduino = Arduino(port=s_name, baud_rate=9600, timeout=2, init_on_wake=True)
        f= arduino.write('1":on')
        print(f)
        arduino.close_connection()
        # ser.close()
        arduino_sim.terminate()

    # def test_arduino(self):
    #     s_name = '/dev/pts/4'
    #     arduino = Arduino(port=s_name, baud_rate=9600, timeout=2, init_on_wake=True)
    #     arduino.write(b'1":on')
    #     arduino.close_connection()


if __name__ == '__main__':
    unittest.main()
