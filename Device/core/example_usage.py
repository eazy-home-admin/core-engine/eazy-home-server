import unittest

from Device.core.device import Device, Register


# Subclass definitions with decorators

@Register.action
class LightPassiveDevice(Device):
    actions = []

    def turn_on(self):
        return "Light turned on."

    def turn_off(self):
        return "Light turned off."


# Ensure this subclass is defined before running tests

@Register.action
class FanPassiveDevice(Device):
    actions = []

    def turn_on(self):
        return "Fan turned on."

    def turn_off(self):
        return "Fan turned off."


class TestDeviceActions(unittest.TestCase):
    def test_actions_are_correctly_registered(self):
        # Test that actions are registered correctly for each subclass
        self.assertIn('turn_on', LightPassiveDevice.actions)
        self.assertIn('turn_off', LightPassiveDevice.actions)
        self.assertIn('turn_on', FanPassiveDevice.actions)
        self.assertIn('turn_off', FanPassiveDevice.actions)
        print(LightPassiveDevice.actions)
        print(FanPassiveDevice.actions)

    def test_action_execution(self):
        # Test executing an action to ensure it works as expected
        light_device = LightPassiveDevice()
        print(light_device.execute_action('turn_on'))
        self.assertEqual(light_device.execute_action('turn_on'), 'Light turned on.')

        # Test executing an action for FanPassiveDevice
        fan_device = FanPassiveDevice()
        self.assertEqual(fan_device.execute_action('turn_on'), 'Fan turned on.')
        self.assertEqual(fan_device.execute_action('dance'), 'Invalid action.')


if __name__ == '__main__':
    unittest.main()
