"""
Comming soon.. A Device Dameon that will handle all the necessary device communication.
Don't worry, There is nothing here to nuke your PC.
"""
import json
import time
from multiprocessing import Queue
import socket

from cryptography.fernet import Fernet

from app import store
from design_patterns.patterns import Singleton
from quick_yaml.DatabaseDaemon import DBDSSocketClient

devices = list()
controllers = list()
conf = store.getConfig()


# Declare a meta class for singleton class

class DeviceDaemon(metaclass=Singleton):
    """Static class to manage daemon"""

    def __init__(self, config_dir, database_socket, db_key, dd_key, dd_socket):
        self.config_dir = config_dir
        self.queue = Queue()
        self.controllers = dict()
        self.non_controllers = dict()
        self.database_socket = database_socket  # String to store database socket location
        self.dbms_client = DBDSSocketClient(database_socket, db_key)
        self.dd_socket = dd_socket
        self.socket = None
        self.dd_key: Fernet = dd_key
        self.db_key: Fernet = db_key

    def start(self):
        """Start the daemon"""

        time.sleep(1)
        self.dbms = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        self.dbms.connect(self.database_socket)

        # Get all device details from dbms
        query = {'type': '$query', 'query': {}}
        self.dbms.send(json.dumps(query).encode())
        device_details = self.dbms.recv(1024).decode()
        device_details = json.loads(device_details)

        # Socket to listen to commands from  Flask API
        self.socket = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        self.socket.bind(self.dd_socket)
        self.socket.listen(5)
        while True:
            # Accept client Connection
            client, address = self.socket.accept()
            print(f"Client connected from {address}")
            # For now print the Client address we will redress this later.
            # Receive the lenght of string
            length = int(client.recv(1024).decode())
            # send ack message to client
            client.send("ack".encode())
            # Receive the string
            data = client.recv(length).decode()

            # Process the data
            results = self.process_command(data, client)
            # send the results length
            client.send(str(len(results)).encode())

            # if ack received, send the results else close connection
            ack = client.recv(1024).decode()
            if ack == 'ack':
                client.sendall(results.encode())
            else:
                client.close()
            # send the results

    def process_command(self, data, client):
        # Dummy command for now.

        ## {type:change_device_state ,device_name:"d1",command:"change_brightness",args:{value:255}}
        ## {device_name:"d1",command:"get_state"}
        ## {device_name:"rgb_lamp",command:"change_color" args{r:255,g:255,b:255}}
        while True:
            ret = {}
            if data['type'] == 'change_device_state':
                ret = self.change_device_state(data)
            elif data['type'] == 'register_device':
                ret = self.register_device(data)
            elif data['type'] == 'remove_device':
                ret = self.remove_device(data.get('id'))
            else:
                ret = "Invalid Command"
            encrypted_response = self.dd_key.encrypt(ret.encode())
            client.send(encrypted_response)

    def register_device(self, data):
        # parse device detals
        # Test device for connection by loading the class
        # Store the details in DBMS
        pass

    def remove_device(self, id):
        # parse device details
        # search for matches
        # delete
        res = {}
        return res

    def get_device_info(self):
        # parse query and return results
        pass

    def update_device_state(self):

        # parse query update the status

        pass

    def get_device_data(self, device_id='all'):
        pass

    def change_device_state(self, data):

        device_name = data.get('device_name')
        command = data.get('command')
        args = data.get('args')
        if device_name is None or command is None:
            return json.dumps({'status': 'device_name or command missing'})

        # get the device record
        command = {'command': '$find', 'table_name': 'devices', 'query': {'name': {'$eq': data['device_name']}}}
        encrypted_data = self.db_key.encrypt(json.dumps(command).encode())
        encrypted_data_length = len(encrypted_data)
        self.dbms_client.send(str(encrypted_data_length).encode())
        response = self.dbms_client.results
        if response is not None:
            data = json.loads(self.db_key.decrypt(response).decode())
            module = data.get('module', None)
            is_controller = data.get('is_controller', False)
            controller_id = data.get('controller_id', None)
            if is_controller and controller_id is not None:
                encrypted_command = self.db_key.encrypt(

                    json.dumps({'command': "$find", '$table_name': 'devices',
                                "$query": {"id": {"$eq": controller_id}}}).encode())
                encrypted_command_length = len(encrypted_command)
                response = self.dbms_client.send(str(encrypted_command_length).encode())

                data = json.loads(self.db_key.decrypt(response).decode())
                if data is not None:
                    ip = data.get('ip', None)
