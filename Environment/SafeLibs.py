"""
Safer version of Libraries to support extensions in restricted environment.
"""
import _io
import contextlib
import os
from typing import re

import cv2
import joblib
import pandas as pd
import yaml

import socket

import magic

from design_patterns.patterns import SingletonNoSubClass, FinalClass


class SafeOS(metaclass=SingletonNoSubClass):

    def __init__(self):
        self._acl_manger = globals().get('acl_manager')
        if self._acl_manger is None:
            raise PermissionError("ACL manager is not available.")
        self._allowed_directories = [os.path.abspath(dirs) for dirs in self._acl_manger.get_allowed_dirs()]

    def __is_path_allowed(self, path):
        """Check if the given path is within the allowed directories."""
        absolute_path = os.path.abspath(path)
        return self._acl_manger.is_path_allowed(absolute_path)

    def list_directory(self, directory):
        """List the contents of a directory, ensuring it's within allowed paths."""
        if not self.is_path_allowed(directory):
            raise PermissionError("Access to this directory is not allowed.")
        try:
            return os.listdir(directory)
        except Exception as e:
            print(f"Error accessing {directory}: {e}")
            return []

    def path_exists(self, path):
        """Check if a path exists, ensuring it's within allowed paths."""
        if not self.is_path_allowed(path):
            raise PermissionError("Access to this path is not allowed.")
        return os.path.exists(path)

    def get_file_size(self, path):
        """Get the size of a file, ensuring it's within allowed paths."""
        if not self.is_path_allowed(path):
            raise PermissionError("Access to this file is not allowed.")
        try:
            return os.path.getsize(path)
        except Exception as e:
            print(f"Error getting size of {path}: {e}")
            return 0

    def create_directory(self, path):
        """Create a new directory, ensuring it's within allowed paths."""
        if not self.is_path_allowed(path):
            raise PermissionError("Creating directories in this location is not allowed.")
        try:
            os.makedirs(path, exist_ok=True)
            return True
        except Exception as e:
            print(f"Error creating directory {path}: {e}")
            return False

    def remove(self, path):
        """Remove a file or directory, ensuring it's within allowed paths."""
        if not self.is_path_allowed(path):
            raise PermissionError("Removing files or directories in this location is not allowed.")
        try:
            if os.path.isdir(path):
                os.rmdir(path)
            else:
                os.remove(path)
            return True
        except Exception as e:
            print(f"Error removing {path}: {e}")
            return False

    def get_current_directory(self):
        """Get the current working directory, ensuring it's within allowed paths."""
        cwd = os.getcwd()
        if self.is_path_allowed(cwd):
            return cwd
        else:
            raise PermissionError("Current directory access is not allowed.")

    def change_directory(self, path):
        """Change the current working directory, ensuring it's within allowed paths."""
        if not self.is_path_allowed(path):
            raise PermissionError("Changing to this directory is not allowed.")
        try:
            os.chdir(path)
            return True
        except Exception as e:
            print(f"Error changing directory to {path}: {e}")
            return False

    def rename(self, old_path, new_path):
        """Rename a file or directory, ensuring both paths are within allowed paths."""
        if not self.is_path_allowed(old_path) or not self.is_path_allowed(new_path):
            raise PermissionError("Renaming in these locations is not allowed.")
        try:
            os.rename(old_path, new_path)
            return True
        except Exception as e:
            print(f"Error renaming {old_path} to {new_path}: {e}")
            return False

    def __getattr__(self, item):
        """Delegate attribute access to the internal DataFrame."""
        raise PermissionError('Access to other OS functions is not allowed.')


class SafeSocket(socket.socket):
    def __init__(self, allowed_dirs, *args, **kwargs):  # pylint: disable=unused-argument
        super().__init__(*args, **kwargs)
        self.allowed_dirs = [os.path.abspath(dir) for dir in allowed_dirs]
        self.mime = magic.Magic(mime=True)

    def is_path_allowed(self, path):
        """Check if the given path is within the allowed directories."""
        absolute_path = os.path.abspath(path)
        return any(absolute_path.startswith(allowed_dir) for allowed_dir in self.allowed_dirs)

    def is_executable_file(self, file_path):
        """Check if a file is executable."""
        return (self.is_executable_by_extension(file_path) or
                self.is_executable_by_magic(file_path) or
                self.is_executable_by_permissions(file_path))

    def is_executable_by_extension(self, file_path):
        executable_extensions = {'.exe', '.sh', '.bat', '.bin', '.command', '.com', '.msi', '.out'}
        _, ext = os.path.splitext(file_path)
        return ext.lower() in executable_extensions

    def is_executable_by_magic(self, file_path):
        file_type = self.mime.from_file(file_path)
        return 'application/x-executable' in file_type or 'application/x-sharedlib' in file_type or 'application/octet-stream' in file_type

    def is_executable_by_permissions(self, file_path):
        return os.access(file_path, os.X_OK)

    def sendfile(self, file, offset=0, count=None):
        """Send a file, ensuring it's within allowed paths and not executable."""
        if not self.is_path_allowed(file):
            raise PermissionError("Access to this file is not allowed.")
        if self.is_executable_file(file):
            raise PermissionError("Sending executable files is not allowed.")
        with open(file, 'rb') as f:
            self.sendfile(f.fileno(), offset, count)

    def send_file(self, file_path):
        """Send a file, ensuring it's within allowed paths and not executable.
        """
        if not self.is_path_allowed(file_path):
            raise PermissionError("Access to this file is not allowed.")
        if self.is_executable_file(file_path):
            raise PermissionError("Sending executable files is not allowed.")
        with open(file_path, 'rb') as f:
            data = f.read()
        self.send(data)

    def recv_file(self, file_path, bufsize):
        """Receive a file, ensuring it's within allowed paths and not executable."""
        if not self.is_path_allowed(file_path):
            raise PermissionError("Access to this file is not allowed.")
        data = self.recv(bufsize)
        with open(file_path, 'wb') as f:
            f.write(data)
        if self.is_executable_file(file_path):
            raise PermissionError("Received file is executable, which is not allowed.")
        return file_path

    def __is_executable_by_extension(self, file_path):
        executable_extensions = {'.exe', '.sh', '.bat', '.bin', '.command', '.com', '.msi', '.out'}
        _, ext = os.path.splitext(file_path)
        return ext.lower() in executable_extensions

    def __is_executable_by_magic(self, file_path):
        file_type = self.mime.from_file(file_path)
        return 'application/x-executable' in file_type or 'application/x-sharedlib' in file_type or 'application/octet-stream' in file_type

    def __is_executable_by_permissions(self, file_path):
        return os.access(file_path, os.X_OK)


class SafePandasDataFrame:
    def __init__(self, data=None, index=None, columns=None, dtype=None, copy=False):
        self._dataframe = pd.DataFrame(data=data, index=index, columns=columns, dtype=dtype, copy=copy)
        self._acl_manager = globals().get('acl_manager')
        if self._acl_manager is None:
            raise PermissionError("ACL manager is not available.")

    def is_path_allowed(self, path):
        """Check if the given path is within the allowed directories."""
        return self._acl_manager.is_path_allowed(path)

    @classmethod
    def read_csv(cls, file_path, *args, **kwargs):
        """Load a CSV file, ensuring it's within allowed paths."""
        instance = cls()
        if not instance.is_path_allowed(file_path):
            raise PermissionError("Access to this file is not allowed.")
        df = pd.read_csv(file_path, *args, **kwargs)
        instance._dataframe = df
        return instance

    @classmethod
    def read_excel(cls, file_path, *args, **kwargs):
        """Load an Excel file, ensuring it's within allowed paths."""
        instance = cls()
        if not instance.is_path_allowed(file_path):
            raise PermissionError("Access to this file is not allowed.")
        df = pd.read_excel(file_path, *args, **kwargs)
        instance._dataframe = df
        return instance

    @classmethod
    def read_json(cls, file_path, *args, **kwargs):
        """Load a JSON file, ensuring it's within allowed paths."""
        instance = cls()
        if not instance.is_path_allowed(file_path):
            raise PermissionError("Access to this file is not allowed.")
        df = pd.read_json(file_path, *args, **kwargs)
        instance._dataframe = df
        return instance

    def to_csv(
            self,
            path_or_buf=None,
            sep=",",
            na_rep="",
            float_format=None,
            columns=None,
            header=True,
            index=True,
            index_label=None,
            mode="w",
            encoding=None,
            compression="infer",
            quoting=None,
            quotechar='"',
            lineterminator=None,
            chunksize=None,
            date_format=None,
            doublequote=True,
            escapechar=None,
            decimal=".",
            errors="strict",
            storage_options=None,
    ) -> str | None:
        """Write the dataframe to a CSV file, ensuring it's within allowed paths."""
        if not self.is_path_allowed(path_or_buf):
            raise PermissionError("Access to this file is not allowed.")
        return self._dataframe.to_csv(
            path_or_buf,
            sep,
            na_rep,
            float_format,
            columns,
            header,
            index,
            index_label,
            mode,
            encoding,
            compression,
            quoting,
            quotechar,
            lineterminator,
            chunksize,
            date_format,
            doublequote,
            escapechar,
            decimal,
            errors,
            storage_options,
        )

    def __getattr__(self, item):
        """Delegate attribute access to the internal DataFrame."""

        return getattr(self._dataframe, item)


class SafeCV2(metaclass=SingletonNoSubClass):
    def __init__(self):
        acl_manger = globals().get('acl_manager')
        if acl_manger is None:
            raise PermissionError("ACL manager is not available.")
        self.allowed_dirs = [os.path.abspath(dirs) for dirs in acl_manger.get_allowed_dirs()]

    def is_path_allowed(self, path):
        """Check if the given path is within the allowed directories."""
        absolute_path = os.path.abspath(path)
        return any(absolute_path.startswith(allowed_dir) for allowed_dir in self.allowed_dirs)

    def imread(self, file_path, flags=cv2.IMREAD_COLOR):
        """Read an image from a file, ensuring it's within allowed paths."""
        if not self.is_path_allowed(file_path):
            raise PermissionError("Access to this file is not allowed.")
        return cv2.imread(file_path, flags)

    def imwrite(self, file_path, img, params=None):
        """Write an image to a file, ensuring it's within allowed paths."""
        if not self.is_path_allowed(file_path):
            raise PermissionError("Access to this file is not allowed.")
        return cv2.imwrite(file_path, img, params)

    def imshow(self, winname, mat):
        """Display an image in a window."""
        return cv2.imshow(winname, mat)

    def waitKey(self, delay=0):
        """Wait for a key event infinitely or for a delay in milliseconds."""
        return cv2.waitKey(delay)

    def destroyAllWindows(self):
        """Destroy all the HighGUI windows."""
        return cv2.destroyAllWindows()

    def __getattr__(self, item):
        """Delegate attribute access to the internal DataFrame."""
        return getattr(cv2, item)


class SafeJoblib(metaclass=SingletonNoSubClass):

    def __init__(self):
        acl_manger = globals().get('acl_manager')
        if acl_manger is None:
            raise PermissionError("ACL manager is not available.")
        self.allowed_dirs = [os.path.abspath(dir) for dir in acl_manger.get_allowed_dirs()]
        del acl_manger

    def __is_path_allowed(self, path):
        """Check if the given path is within the allowed directories."""
        absolute_path = os.path.abspath(path)
        return any(absolute_path.startswith(allowed_dir) for allowed_dir in self.allowed_dirs)

    def __is_model(self, joblib_content):
        """
        Supports basic check for joblib_content
        """
        model_attribute = ('fit', 'predict', 'score')
        return any(attr in model_attribute for attr in joblib_content)

    def load(self, file_path, *args, **kwargs):
        """Load a joblib file, ensuring it's within allowed paths."""
        if not self.__is_path_allowed(file_path):
            raise PermissionError("Access to this file is not allowed.")
        data = joblib.load(file_path, *args, **kwargs)
        if self.__is_model(data):
            raise PermissionError("Access to this file is not allowed.")
        return data

    def dump(self, obj, file_path, *args, **kwargs):
        """Dump a joblib file, ensuring it's within allowed paths.

        Parameters:
            obj: object to be saved
            file_path: file path to be saved.
            *args: arguments for joblib.dump
            **kwargs: keyword arguments for joblib.dump
        """
        if not self.__is_path_allowed(file_path):
            raise PermissionError("Access to this file is not allowed.")
        data = joblib.dump(obj, file_path, *args, **kwargs)
        if self.__is_model(data):
            raise PermissionError("Access to this file is not allowed.")
        return data

    def __getattr__(self, item):
        raise PermissionError("""Access to other joblib functions is not allowed.
         Add it to trusted extensions in the ACL manager.""")


class SafeFile(metaclass=FinalClass):
    def __init__(self, file_path):
        acl_manager = globals().get('acl_manager')
        if acl_manager is None:
            raise PermissionError("ACL manager is not available.")
        self.allowed_dirs = [os.path.abspath(dir) for dir in acl_manager.get_allowed_directories()]
        del acl_manager
        self._file_path = os.path.abspath(file_path)
        if not self.is_path_allowed(self._file_path):
            raise PermissionError("Access to this file path is not allowed.")
        self._file = None

    def is_path_allowed(self, path):
        """Check if the given path is within the allowed directories."""
        absolute_path = os.path.abspath(path)
        return any(absolute_path.startswith(allowed_dir) for allowed_dir in self.allowed_dirs)

    def read(self):
        """Read the file if the path is allowed."""
        if not self.is_path_allowed(self._file_path):
            raise PermissionError("Access to this file path is not allowed.")
        with open(self._file_path, 'r') as file:
            return file.read()

    def read_line(self):
        """Read the file if the path is allowed."""
        if not self.is_path_allowed(self._file_path):
            raise PermissionError("Access to this file path is not allowed.")
        with open(self._file_path, 'r') as file:
            return file.readline()

    def read_lines(self,max_lines=1):
        """Read the file if the path is allowed."""
        if not self.is_path_allowed(self._file_path):
            raise PermissionError("Access to this file path is not allowed.")
        with open(self._file_path, 'r') as file:
            return file.readlines(max_lines)

    def write(self, content):
        """Write to the file if the path is allowed."""
        if not self.is_path_allowed(self._file_path):
            raise PermissionError("Access to this file path is not allowed.")
        with open(self._file_path, 'w') as file:
            file.write(content)

    def delete(self):
        """Delete the file if the path is allowed."""
        if not self.is_path_allowed(self._file_path):
            raise PermissionError("Access to this file path is not allowed.")
        os.remove(self._file_path)

    def get_size(self):
        """Get the size of the file if the path is allowed."""
        if not self.is_path_allowed(self._file_path):
            raise PermissionError("Access to this file path is not allowed.")
        return os.path.getsize(self._file_path)

    def rename(self, new_path):
        """Rename the file if both paths are allowed."""
        new_path = os.path.abspath(new_path)
        if not self.is_path_allowed(new_path):
            raise PermissionError("Access to the new file path is not allowed.")
        os.rename(self._file_path, new_path)
        self._file_path = new_path

    def __enter__(self):
        self._file = open(self._file_path, 'r+')
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self._file:
            self._file.close()

    def read_context(self):
        """Read the file using context manager if the path is allowed."""
        if not self.is_path_allowed(self._file_path):
            raise PermissionError("Access to this file path is not allowed.")
        return self._file.read()

    def write_context(self, content):
        """Write to the file using context manager if the path is allowed."""
        if not self.is_path_allowed(self._file_path):
            raise PermissionError("Access to this file path is not allowed.")
        self._file.write(content)


    def find(self, pattern):
        """Find the file if the path is allowed."""
        if not self.is_path_allowed(self._file_path):
            raise PermissionError("Access to this file path is not allowed.")
        contents = self._file.read()
        return re.findall(pattern,contents)

    def seek(self, offset=0):
        """Seek the file if the path is allowed."""
        if not self.is_path_allowed(self._file_path):
            raise PermissionError("Access to this file path is not allowed.")
        self._file.seek(offset)


