import unittest
import os
import yaml
from unittest.mock import patch, mock_open
from Environment.acl_manager import ACLManager as ACLM
class TestACL(unittest.TestCase):

    def setUp(self):
        self.file_path = '/tmp/acl.yaml'
        self.manager = ACLM(config_path='/tmp/acl.yaml', read_only=False, content_if_not_exists={
            'Allowed Directories': ['/tmp/'],
            'Allowed Libraries': ['scipy', 'matplotlib', 'extension', 'math', 'random', 'sklearn', 'code_p.py', 'numpy',
                                  'scipy', 'seaborn', 'keras', 'tensorflow'],
            'Trusted Extensions': ['Arduino'],
            'Trusted URLs': ['127.0.0.1']
        }
                            )

    def test_acl_r(self):
        read_manager = self.manager.read_only_clone()
        # check if this raises exception
        with self.assertRaises(PermissionError):
            read_manager.add_trusted_url('http://google.com')

    def test_add(self):
        self.manager.add_allowed_library('sklearn')
        assert self.manager.is_allowed_library('sklearn')
        self.manager.add_trusted_extension('Arduino')
        assert self.manager.is_trusted_extension('Arduino')
        self.manager.add_trusted_url('http://google.com')
        assert self.manager.is_trusted_url('http://google.com')

    def test_remove(self):
        self.manager.remove_allowed_library('sklearn')
        assert not self.manager.is_allowed_library('sklearn')
        self.manager.remove_trusted_extension('Arduino')
        assert not self.manager.is_trusted_extension('Arduino')
        self.manager.remove_trusted_url('http://google.com')
        assert not self.manager.is_trusted_url('http://google.com')

    def tearDown(self):
        if os.path.exists(self.file_path):
            os.remove(self.file_path)



