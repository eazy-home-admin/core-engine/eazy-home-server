import TestDeviceAPI as API

def testAPIInit(self):
    self.pointer = API.deviceAPI(self.host, self.port)

def testAPIMain(command):
    if command.action == "set":
        if command.label == "brightness":
            command.subject.pointer.setBrightness(command.value)
            print(command.subject.pointer.getBrightness())

def ping(self):
    return self.pointer.ping()



api = {

    "main" : testAPIMain,
    "init" : testAPIInit,
    "ping" : ping
    
}