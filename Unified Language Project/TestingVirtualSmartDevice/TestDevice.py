import socket
 
def Main():

    brightness = 0

    #Fake network garbage that would be on the device itself

    host = ""
    port = 12345

    print("start")
    
    s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)

    s.bind((host,port))

    s.listen(5)

    #Fake device code that handles stuff 

    c, addr = s.accept()

    while True:        

        data = c.recv(1024)
        if data:
            print('Received from the server :',str(data.decode('ascii')))
            if str(data.decode('ascii')) == "ping":
                send = "pong"
                c.send(send.encode("ascii"))
            if str(data.decode('ascii')) == "get brightness":
                send = str(brightness)
                c.send(send.encode('ascii'))
            if "set brightness" in str(data.decode('ascii')):
               brightness = int(data.decode('ascii').split(' ')[2])
  
        
  
    s.close()
  
if __name__ == '__main__':
    Main()
