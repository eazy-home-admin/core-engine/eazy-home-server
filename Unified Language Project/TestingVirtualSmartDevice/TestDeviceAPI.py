# import socket programming library
import socket

class deviceAPI:
    
    def __init__ (self, host, port):
        self.host = host
        self.port = port
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.connect((host, port))

    def send(self, data):
        self.s.send(data.encode("ascii"))

    def listen(self, size):
        data = self.s.recv(size) 
        return str(data.decode('ascii'))

    def query(self, data):
        self.send(data)
        return(self.listen(1024))

    def ping(self):
        return(self.query("ping"))

    def setBrightness(self, val):
        self.send("set brightness " + str(val))
    
    def getBrightness(self):
       return(self.query("get brightness"))


