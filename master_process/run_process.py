import os

import yaml
from cryptography import fernet

from Device.daemon import DeviceDaemon
from Environment.config_manager import Configuration
from app import *
from quick_yaml.DatabaseDaemon import DataBaseSocketDaemon

# global variables


dbd = None
dd = None

global config

config = Configuration()

config.load_configuration()


#  New BootStrap process to start the daemon and process


# Create aes keys  using fernet

dbd_key = fernet.Fernet.generate_key()
dd_key = fernet.Fernet.generate_key()



